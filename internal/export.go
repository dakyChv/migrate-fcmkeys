package internal

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"cloud.google.com/go/firestore"
)

func (s *Service) ExportToJson(ctx context.Context) error {
	_, err := ToJson(ctx, s.Firestore, "")
	if err != nil {
		return fmt.Errorf("ExportJson(): %w", err)
	}
	return nil
}

func ToJson(ctx context.Context, db *firestore.Client, nextPage string) (string, error) {
	ref := db.Collection("users")
	query := ref.OrderBy("jdbCif", firestore.Desc).Limit(pageSize)
	if nextPage != "" {
		dsnap, err := ref.Doc(nextPage).Get(ctx)
		if err != nil {
			return "", fmt.Errorf("ToJson(): %w", err)
		}
		query = query.StartAfter(dsnap)
	}
	dsnap, err := query.Documents(ctx).GetAll()
	if err != nil {
		return "", fmt.Errorf("ToJson(): %w", err)
	}

	if len(dsnap) == 0 {
		return "", nil
	}

	newFCMKeys := make([]*FCMKey, 0)
	for _, d := range dsnap {
		oldFcmkey := new(OldFCMKey)
		if err := d.DataTo(&oldFcmkey); err != nil {
			return "", fmt.Errorf("ToJson(): %w", err)
		}

		tokens := make(map[string]FCMInfo)
		for t, v := range oldFcmkey.FCMKeys {
			tokens[t] = FCMInfo{
				Time: v,
			}
		}

		newFCMKeys = append(newFCMKeys, &FCMKey{
			CIF: func() string {
				if oldFcmkey.CIF == "" {
					oldFcmkey.CIF = d.Ref.ID
				}
				return oldFcmkey.CIF
			}(),
			UID:        oldFcmkey.UID,
			BadgeCount: oldFcmkey.BadgeCount,
			Setting:    oldFcmkey.Setting,
			FCMKeys:    tokens,
		})
	}

	file, err := json.MarshalIndent(newFCMKeys, "", " ")
	if err != nil {
		return "", fmt.Errorf("ToJson(): %w", err)
	}

	err = os.MkdirAll("../../users", os.ModePerm)
	if err != nil {
		// handle error
		return "", fmt.Errorf("ToJson(): %w", err)
	}

	if err = ioutil.WriteFile(fmt.Sprintf("../../users/%d.json", time.Now().UnixNano()), file, 0644); err != nil {
		return "", fmt.Errorf("ToJson(): %w", err)
	}

	var newNextPage string
	if len(newFCMKeys) == pageSize {
		newNextPage = newFCMKeys[len(newFCMKeys)-1].CIF
	}

	// Stop recusion if no page.
	if newNextPage == "" {
		log.Println("Stop recursion")
		return "", nil
	}

	return ToJson(ctx, db, newNextPage)
}

func (s *Service) ExportToTxt(ctx context.Context) error {
	log.Println("start")
	t := time.Now()

	err := os.MkdirAll("../../users", os.ModePerm)
	if err != nil {
		// handle error
		return fmt.Errorf("ToTxt(): %w", err)
	}

	f, err := os.Create("../../users/users.txt")
	if err != nil {
		return fmt.Errorf("ExportToTxt(): %w", err)
	}
	defer f.Close()

	// Open the file in append mode
	file, err := os.OpenFile("../../users/users.txt", os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		return fmt.Errorf("ExportToTxt(): %w", err)
	}
	defer file.Close()

	_, err = ToTxt(ctx, s.Firestore, file, "")
	if err != nil {
		return fmt.Errorf("ExportToTxt(): %w", err)
	}

	log.Printf("average time: %v", time.Since(t))
	log.Println("end")

	return nil
}

func ToTxt(ctx context.Context, db *firestore.Client, file *os.File, nextPage string) (string, error) {
	ref := db.Collection("users")
	query := ref.OrderBy("jdbCif", firestore.Desc).Limit(pageSize)
	if nextPage != "" {
		dsnap, err := ref.Doc(nextPage).Get(ctx)
		if err != nil {
			return "", fmt.Errorf("ToTxt(): %w", err)
		}
		query = query.StartAfter(dsnap)
	}
	dsnap, err := query.Documents(ctx).GetAll()
	if err != nil {
		return "", fmt.Errorf("ToTxt(): %w", err)
	}

	if len(dsnap) == 0 {
		return "", nil
	}

	for _, d := range dsnap {
		oldFcmkey := new(OldFCMKey)
		if err := d.DataTo(&oldFcmkey); err != nil {
			return "", fmt.Errorf("ToTxt(): %w", err)
		}

		fcminfo := make(map[string]FCMInfo)
		for t, v := range oldFcmkey.FCMKeys {
			fcminfo[t] = FCMInfo{
				Time: v,
			}
		}

		newFcmkey := FCMKey{
			CIF: func() string {
				if oldFcmkey.CIF == "" {
					oldFcmkey.CIF = d.Ref.ID
				}
				return oldFcmkey.CIF
			}(),
			UID:        oldFcmkey.UID,
			BadgeCount: oldFcmkey.BadgeCount,
			Setting:    oldFcmkey.Setting,
			FCMKeys:    fcminfo,
		}

		// Seek to the end of the file
		_, err = file.Seek(0, os.SEEK_END)
		if err != nil {
			return "", fmt.Errorf("ToTxt(): %w", err)
		}

		b, err := json.Marshal(newFcmkey)
		if err != nil {
			return "", fmt.Errorf("ToTxt(): %w", err)
		}

		// Write data to the file
		_, err = file.WriteString(fmt.Sprintf("%s\n", b))
		if err != nil {
			fmt.Println("Failed to write data to file:", err)
			return "", fmt.Errorf("ToTxt(): %w", err)
		}
	}

	var newNextPage string
	if len(dsnap) == pageSize {
		newNextPage = dsnap[len(dsnap)-1].Ref.ID
	}

	// Stop recusion if no page.
	if newNextPage == "" {
		log.Println("Stop recursion")
		return "", nil
	}

	return ToTxt(ctx, db, file, newNextPage)
}
