package internal

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	firestore "cloud.google.com/go/firestore"
)

func (s *Service) MigrateDownOnEmulator(ctx context.Context) error {
	t := time.Now()
	log.Println("start")

	if err := s.migrateDownFMCKeyBulk(ctx); err != nil {
		return fmt.Errorf("MigrateDownOnEmulator(): %w", err)
	}

	log.Printf("Average time: %v", time.Since(t))
	log.Println("end")

	return nil
}

func (s *Service) migrateDownFMCKeyBulk(ctx context.Context) error {
	file, err := os.OpenFile("../../users/users.txt", os.O_RDONLY, os.ModePerm)
	if err != nil {
		return fmt.Errorf("migrateDownFMCKeyBulk(): %w", err)
	}
	defer file.Close()

	sc := bufio.NewScanner(file)

	fcmkeysPerBulk := 300

	fcmkeys := make([]*FCMKey, 0)
	for sc.Scan() {
		fcmkey := new(FCMKey)
		if err := json.Unmarshal([]byte(sc.Text()), &fcmkey); err != nil {
			return fmt.Errorf("migrateDownFMCKeyBulk(): %w", err)
		}
		fcmkeys = append(fcmkeys, fcmkey)
		if len(fcmkeys) == fcmkeysPerBulk {
			err = migrateDownFCMKeyBulk(ctx, s.Firestore, fcmkeys)
			// Reset the buffer for the next bulk
			fcmkeys = fcmkeys[:0]
		}
		if err != nil {
			return fmt.Errorf("migrateDownFMCKeyBulk(): %w", err)
		}
	}

	// Update the remaining fcmkeys in the last bulk
	if len(fcmkeys) > 0 {
		err = migrateDownFCMKeyBulk(ctx, s.Firestore, fcmkeys)
	}
	if err != nil {
		return fmt.Errorf("migrateDownFMCKeyBulk(): %w", err)
	}

	if err = sc.Err(); err != nil {
		return fmt.Errorf("migrateDownFMCKeyBulk(): %w", err)
	}

	return nil
}

func migrateDownFCMKeyBulk(ctx context.Context, db *firestore.Client, fcmkeys []*FCMKey) error {
	batch := db.Batch()
	ref := db.Collection("users")

	for _, f := range fcmkeys {
		keys := make(map[string]time.Time, 0)
		for k, v := range f.FCMKeys {
			keys[k] = v.Time
		}
		batch.Set(ref.Doc(f.CIF), OldFCMKey{
			CIF:        f.CIF,
			UID:        f.UID,
			BadgeCount: f.BadgeCount,
			Setting:    f.Setting,
			FCMKeys:    keys,
		})
	}

	_, err := batch.Commit(ctx)
	if err != nil {
		return fmt.Errorf("migrateDownFCMKeyBulk(): %w", err)
	}

	return nil
}
