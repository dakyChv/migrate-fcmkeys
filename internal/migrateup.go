package internal

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"io/fs"
	"io/ioutil"
	"log"
	"os"
	"sync"
	"time"

	"cloud.google.com/go/firestore"
)

var (
	wg sync.WaitGroup
)

func (s *Service) MigrateFCMKeysByJson(ctx context.Context) error {
	files, err := ioutil.ReadDir("../../users")
	if err != nil {
		return fmt.Errorf("MigrateFCMKeysByJson(): %w", err)
	}
	// List file
	for _, file := range files {
		err = s.updateFCMKeysByJson(ctx, s.Firestore, file)
		if err != nil {
			return fmt.Errorf("MigrateFCMKeysByJson(): %w", err)
		}
		log.Printf("Success, file %s", file.Name())
	}
	return nil
}

func (s *Service) updateFCMKeysByJson(ctx context.Context, db *firestore.Client, file fs.FileInfo) error {
	// split data on file to small bulk
	spliterator := 250

	b, err := ioutil.ReadFile(fmt.Sprintf("../../users/%s", file.Name()))
	if err != nil {
		return fmt.Errorf("updateFCMKeysByJson(): %w", err)
	}
	fcmkeys := make([]*FCMKey, 0)
	if err := json.Unmarshal(b, &fcmkeys); err != nil {
		return fmt.Errorf("updateFCMKeysByJson(): %w", err)
	}

	if len(fcmkeys) == 0 {
		return nil
	}

	// Paginate
	page := len(fcmkeys) / spliterator

	// Batched writing each page with goroutine group.
	wg.Add(page)
	for i := 1; i <= page; i++ {
		end := spliterator * i
		start := end - spliterator

		pfcmkey := fcmkeys[start:end]
		if i == page {
			pfcmkey = fcmkeys[start:]
		}

		go func(ctx context.Context, fcmkeys []*FCMKey) {
			defer wg.Done()

			batch := db.Batch()
			cref := db.Collection("users")
			for _, v := range fcmkeys {
				dref := cref.Doc(v.CIF)
				batch.Set(dref, v)
			}

			_, err := batch.Commit(ctx)
			if err != nil {
				log.Printf("updateFCMKeysByJson(): %v", err)
			}
		}(ctx, pfcmkey)
	}
	wg.Wait()

	return nil
}

// TODO: Single
func (s *Service) MigrateEachFCMKey(ctx context.Context) error {
	if err := s.migrateEachFCMKey(ctx); err != nil {
		return fmt.Errorf("MigrateEachFCMKey(): %w", err)
	}
	return nil
}

func (s *Service) migrateEachFCMKey(ctx context.Context) error {
	f, err := os.OpenFile("../../users/users.txt", os.O_RDONLY, os.ModePerm)
	if err != nil {
		return fmt.Errorf("migrateEachFCMKey(): %w", err)
	}
	defer f.Close()

	sc := bufio.NewScanner(f)
	count := 1
	for sc.Scan() {
		if err := updateFCMKey(ctx, s.Firestore, sc.Text()); err != nil {
			return fmt.Errorf("migrateEachFCMKey(): %w", err)
		}
		fmt.Println(count)
		count++
	}

	if err := sc.Err(); err != nil {
		return fmt.Errorf("migrateEachFCMKey():  %w", err)
	}

	return nil
}

func updateFCMKey(ctx context.Context, db *firestore.Client, r string) error {
	fcmkey := new(FCMKey)
	if err := json.Unmarshal([]byte(r), &fcmkey); err != nil {
		return fmt.Errorf("updateFCMKey(): %w", err)
	}

	ref := db.Collection("users").Doc(fcmkey.CIF)
	_, err := ref.Set(ctx, fcmkey)
	if err != nil {
		return fmt.Errorf("updateFCMKey(): %w", err)
	}

	return nil
}

// TODO: Bulk
func (s *Service) MigrateFCMKeyBulk(ctx context.Context) error {
	t := time.Now()
	log.Println("start")

	if err := s.migrateFMCKeyBulk(ctx); err != nil {
		return fmt.Errorf("MigrateFCMKeyBulk(): %w", err)
	}

	log.Printf("Average time: %v", time.Since(t))
	log.Println("end")

	return nil
}

func (s *Service) migrateFMCKeyBulk(ctx context.Context) error {
	file, err := os.OpenFile("../../users/users.txt", os.O_RDONLY, os.ModePerm)
	if err != nil {
		return fmt.Errorf("migrateFMCKeyBulk(): %w", err)
	}
	defer file.Close()

	sc := bufio.NewScanner(file)

	fcmkeysPerBulk := 300

	fcmkeys := make([]*FCMKey, 0)
	for sc.Scan() {
		fcmkey := new(FCMKey)
		if err := json.Unmarshal([]byte(sc.Text()), &fcmkey); err != nil {
			return fmt.Errorf("migrateFMCKeyBulk(): %w", err)
		}
		fcmkeys = append(fcmkeys, fcmkey)
		if len(fcmkeys) == fcmkeysPerBulk {
			err = updateFCMKeyBulk(ctx, s.Firestore, fcmkeys)
			// Reset the buffer for the next bulk
			fcmkeys = fcmkeys[:0]
		}
		if err != nil {
			return fmt.Errorf("migrateFMCKeyBulk(): %w", err)
		}
	}

	// Update the remaining fcmkeys in the last bulk
	if len(fcmkeys) > 0 {
		err = updateFCMKeyBulk(ctx, s.Firestore, fcmkeys)
	}
	if err != nil {
		return fmt.Errorf("migrateFMCKeyBulk(): %w", err)
	}

	if err = sc.Err(); err != nil {
		return fmt.Errorf("migrateFMCKeyBulk(): %w", err)
	}

	return nil
}

func updateFCMKeyBulk(ctx context.Context, db *firestore.Client, fcmkeys []*FCMKey) error {
	batch := db.Batch()
	ref := db.Collection("users")

	for _, f := range fcmkeys {
		batch.Set(ref.Doc(f.CIF), f)
	}

	_, err := batch.Commit(ctx)
	if err != nil {
		result, err := json.Marshal(fcmkeys)
		if err != nil {
			return fmt.Errorf("updateFCMKeyBulk(): %w", err)
		}
		log.Println(string(result))
		return fmt.Errorf("updateFCMKeyBulk(): %w", err)
	}

	return nil
}
