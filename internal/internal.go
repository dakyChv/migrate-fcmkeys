package internal

import (
	"time"

	firestore "cloud.google.com/go/firestore"
)

var (
	pageSize = 1000
)

type Setting struct {
	Monetary bool `json:"monetary" firestore:"monetary"`
	General  bool `json:"general" firestore:"general"`
}

type FCMInfo struct {
	DeviceID string    `json:"deviceId" firestore:"deviceId"`
	Time     time.Time `json:"time" firestore:"time"`
}

type OldFCMKey struct {
	CIF        string               `json:"jdbCif" firestore:"jdbCif"`
	UID        string               `json:"userId" firestore:"userId"`
	BadgeCount int32                `json:"badgeCount" firestore:"badgeCount"`
	Setting    *Setting             `json:"setting" firestore:"notificationSetting"`
	FCMKeys    map[string]time.Time `json:"tokens" firestore:"tokens"`
}

type FCMKey struct {
	CIF        string             `json:"jdbCif" firestore:"jdbCif"`
	UID        string             `json:"userId" firestore:"userId"`
	BadgeCount int32              `json:"badgeCount" firestore:"badgeCount"`
	Setting    *Setting           `json:"setting" firestore:"notificationSetting"`
	FCMKeys    map[string]FCMInfo `json:"tokens" firestore:"tokens"`
}

type Service struct {
	Firestore *firestore.Client
}
