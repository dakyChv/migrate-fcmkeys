package main

import (
	"context"
	"log"
	"os"
	"os/signal"

	"notificationmigrate/internal"

	"cloud.google.com/go/firestore"
	firebase "firebase.google.com/go/v4"
)

var (
	gcpProject = os.Getenv("GCP_PROJECT")
	modeEnv    = os.Getenv("ENV_MODE")

	firestoreClient *firestore.Client
	svc             *internal.Service
)

func main() {
	ctx := context.Background()

	ctx, cancel := signal.NotifyContext(ctx, os.Interrupt, os.Kill)
	defer cancel()

	if modeEnv == "dev" {
		// Set FIRESTORE_EMULATOR_HOST environment variable.
		err := os.Setenv("FIRESTORE_EMULATOR_HOST", "localhost:9000")
		if err != nil {
			// TODO: Handle error.
		}
	}

	app, err := firebase.NewApp(ctx, &firebase.Config{
		ProjectID: gcpProject,
	})
	if err != nil {
		log.Printf("Failed to New app: %v", err)
	}

	firestoreClient, err = app.Firestore(ctx)
	if err != nil {
		log.Printf("Failed to New firestore client instance: %v", err)
	}

	svc = &internal.Service{
		Firestore: firestoreClient,
	}

	if err := svc.MigrateDownOnEmulator(ctx); err != nil {
		log.Printf("Failed to clone users: %v", err)
	}

	log.Println("control + c to exits")
	<-ctx.Done()
}
